const {
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_HOSTNAME,
  MONGO_PORT,
  MONGO_DB,
  MONGO_TEST_USERNAME,
  MONGO_TEST_PASSWORD,
  MONGO_TEST_DB,
  MONGO_TEST_HOSTNAME
} = process.env;

db.createUser(
  {
      user: MONGO_USERNAME,
      pwd: MONGO_PASSWORD,
      roles: [
          {
              role: "readWrite",
              db: MONGO_DB
          }
      ]
  }
)


db = db.getSiblingDB(MONGO_TEST_DB);
db.createUser(
  {
    user: MONGO_TEST_USERNAME,
    pwd: MONGO_TEST_PASSWORD,
    roles: [
      {
          role: "readWrite",
          db: MONGO_TEST_DB
      }
    ]
  }
);
