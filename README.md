# Proyecto
Challenge solicitado por Mercadolibre para proceso de postulación.

## Detalle Challenge
Desarrollar un servicio que permita:
- Dada una URL larga, mi servicio me tiene que devolver una URL corta.
- Dada una URL corta, mi siervicio me tiene que devolver la URL larga original.
- Puedan obtenerse estadísticas de las URLs que utilizan este servicio.
- Puedan manejarse solicitudes a gran escala.
- El 90% de todas las solicitudes puedan responderse en menos de 10 ms.
- Puedan borrarse las URLs cortas necesarias.
- Y lógicamente, que el usuario navegue hacia la URL larga cuando ingresa una url corta válida en su navegador :)

En MercadoLibre la escala es importante, así que piensa una solución que pueda escalar hasta, al menos, 50.000 peticiones por segundo.

De igual forma, la creación de una URL corta debe ser un proceso que tarde poco (del orden de 1 segundo).

## Levantar proyecto
Para levantar el proyecto debe estar instalado docker, si no lo está seguir instrucciones desde https://docs.docker.com/install/

### Entorno
La configuración base del proyecto se realiza mediante el archivo `.env`, por lo que debemos copiar el archivo `.env-example` a `.env`
```
cp .env-example .env
```
*modificar archivo .env con los valores necesarios para funcionar en equipo local*

### Base de datos
La conexión a la base de datos se debe realizar en el archivo .env ubicado en la raiz del proyecto.

Ejecutar el siguiente comando en la raiz del proyecto
```
docker-compose up -d
```
### Ejecución
El comando `docker-compose up -d` comenzará a descargar e instalar todas las dependencias necesarias, una vez que haya terminado se puede ejecutar el proyecto de 2 maneras:

**1. Docker**
```
docker-compose exec web npm run dev
```
Este comando no instalará nada extra en el equipo local, solo en el docker.

**2. Desde la maquina**

Para ejecutar con los servicios de la maquina debe ingresar lo siguiente:
```
npm i nodemon -g
npm i
npm run dev
```
El archivo ML_challenge.postman_collection.json puede ser importado en postman para tener un collection de ejemplo.
### Tests
La ejecución de los tests se debe realizar al menos con la base de datos funcionando.
Para correr los tests ejecutar el siguiente comando:

```
docker-compose exec web npm run test
```

## Endpoints
### Acortador de URL (POST)
Servicio recibe un request POST con el parámetro "url" el cual contiene la url a acortar, la respuesta de este servicio es la url corta según el formato establecido en el archivo `.env` (URL_SHORT_BASE y URL_LENGTH)

Al acortar una url se guarda un registro asociado a la url larga y la url corta generada.

### Ir a url (GET)
Endpoint lee la url completa y toma el valor después del nombre del host, por ejemplo si se ingresa http://localhost/XXYYZZ internamente se obtiene la URL larga relacionada a XXYYZZ

Este endpoint genera un registro de log para luego obtener un conteo de la cantidad de visitas a esa URL.

### Data de una url (GET)
Endpoint lee la url completa y toma el valor después del nombre del host, por ejemplo si se ingresa http://localhost/XXYYZZ/data internamente se obtiene la URL larga relacionada a XXYYZZ

Este endpoint **NO** genera un registro de log.

### Eliminar URL (DELETE)
Endpoint que recibe métodos tipo `DELETE`, con el mismo formato que el endpoint para ir a la URL, por ejemplo si se ingresa http://localhost/XXYYZZ internamente se obtiene el registro asociado a XXYYZZ y se elimina de la base de datos.

### Estadísticas URL (GET /logs)
Endpoint que devuelve los tipos de estadísticas que tiene habilitado el sistema, para ver un tipo de log debe usar el key que devuelve dentro del objeto de respuesta.

### Estadísticas URL (GET /logs/:type)
Endpoint que devuelve las estadisticas asociadas a un tipo de log, las opciones disponibles son: `visit`, `enabled`, `deleted`
