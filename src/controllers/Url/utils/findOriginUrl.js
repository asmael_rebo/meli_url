const { UrlModel } = require('../../../models/')
/**
 * @typedef {import('../../../models/types').ShorterUrlAttributes} ShorterUrlAttributes
 */
/**
 * Busca en base a la url de origen.
 * @param {string} value - Url a buscar.
 * @example
 * const getUrl = await findOriginUrl(urlOrigin)
 * @return {Promise<ShorterUrlAttributes>} Url corta
 */
module.exports = async (value) => {
  return await UrlModel.findOne({ url_origin: value }).exec();
}
