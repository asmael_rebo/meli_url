const findOriginUrl = require('./findOriginUrl')
const findShortUrl = require('./findShortUrl')
const generateShortUrl = require('./generateShortUrl')

module.exports = {
  findOriginUrl,
  findShortUrl,
  generateShortUrl
}
