const { describe } = require('mocha')
const { expect } = require('chai')
const action = require('./generateShortUrl')
const { URL_MAX_TRIES } = process.env;


describe('controllers/Url/utils/validateFields', () => {
  it('expect an error', async () => {
    try {
      await action(URL_MAX_TRIES+1)
    } catch (err) {
      expect(err.message).to.equals('No se pudo generar la url por reintentos')
    }
  })
})
