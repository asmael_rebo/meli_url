const { UrlModel } = require('../../../models/')
/**
 * @typedef {import('../../../models/types').ShorterUrlAttributes} ShorterUrlAttributes
 */
/**
 * Busca en base a la url corta.
 * @param {string} value - Url a buscar.
 * @example
 * const getUrl = await findShortUrl(urlShort)
 * @return {Promise<ShorterUrlAttributes>} Url corta
 */
module.exports = async (value) => {
  return await UrlModel.findOne({ url_shorter: value }).exec();
}
