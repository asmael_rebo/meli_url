const findShortUrl = require('./findShortUrl')
const { shorterUrl } = require('../../../utils/')
const { URL_MAX_TRIES } = process.env;

/**
 * Genera la url corta, se realiza una validación para que en el poco probable caso esté repetido el hash ingresado.
 * @param {number} [initTry=1] - Comienzo de conteo para los reintentos de generación de url
 * @example
 * const shortUrl = await generateShortUrl(1)
 * @return {Promise<string>} Url corta
 */
module.exports = async (initTry = 1) => {
  const shortUrl = shorterUrl()
  const exist = await findShortUrl(shortUrl);
  if (initTry >= URL_MAX_TRIES) {
    throw new Error('No se pudo generar la url por reintentos');
  }
  if (!exist) {
    return shortUrl;
  } else {
    initTry++;
    await generateShortUrl(initTry)
  }
}
