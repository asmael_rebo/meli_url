const redis = require('../../../redis')
const { LogUrl } = require('../../../models/')
const { findShortUrl } = require('../utils')
const { responseAction } = require('../../../utils')
/**
 * @typedef {import('../../../utils/types').ResponseAction} ResponseAction
 */
/**
 * Guarda Log asociado a la visita
 * @param {any} data Data a guardar en el log
 * @example
 * const log = saveLog(data)
 * @returns {Promise<boolean>} True/False
 */
const saveLog = async(data) => {
  const saveLog = await new LogUrl(data).save()
  return saveLog
}
/**
 * Devuelve url corta en base a url larga.
 * @example
 * const redirect = redirect(urlCorta)
 * @returns {Promise<ResponseAction>} Url larga
 */
module.exports = async (shortUrl) => {
  try {
    /** Mensaje por defecto */
    const message = 'Url obtenida con éxito.'
    /** Url a redireccionar */
    let dataUrl = await redis.get(shortUrl)
    /** Data a guardar en log */
    const dataLog = {
      url_shorter: shortUrl,
      log_type: 'visit'
    }
    // Si encuentra dataUrl en redis, se redirecciona a esa url, sino se busca en base de datos en caso que no se haya escrito en redis.
    if (dataUrl) {
      dataLog.url_origin = dataUrl
    } else {
      // Se busca la url en la base de datos.
      const getUrl = await findShortUrl(shortUrl)
      if (!getUrl) {
        return responseAction(404, 'Url no encontrada')
      } else {
        dataUrl = getUrl.url_origin
        dataLog.url_origin = dataUrl
      }
    }
    if (dataUrl) {
      const savedLog = await saveLog(dataLog)
      if (!savedLog) {
        message += 'No se guardo el log.'
      }
      return responseAction(301, message, { data: {
        url_origin: dataUrl
      }})
    } else {
      return responseAction(404, 'Url no encontrada')
    }
  } catch(error) {
    throw new Error('error al redireccionar a url')
  }
}
