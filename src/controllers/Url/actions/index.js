const create = require('./create')
const deleteUrl = require('./deleteUrl')
const show = require('./show')
const redirect = require('./redirect')

module.exports = {
  create,
  deleteUrl,
  redirect,
  show
}
