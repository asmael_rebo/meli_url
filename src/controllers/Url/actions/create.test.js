'use stricts'
const { describe } = require('mocha')
const { expect } = require('chai')
const create = require('./create')

describe('controllers/UrlController/actions/create', () => {
  const dataUrl = {
    url_origina: 'http://localhost/slashslashlaurlmaslargadelmundo',
    url_shorter: 'http://shorter.cl/aabbcc'
  }

  it('expect an error', async () => {
    try {
      await create('laraira')
    } catch (err) {
      expect(err.message).to.equals('Formato url incorrecto')
    }
  })

  it('expect a success, new url', async () => {
    dataUrl.url_origin = dataUrl.url_origina
    const data = await create(dataUrl.url_origin)
    expect(data.code).to.equals(200)
  })

  it('expect a success, get previous url', async () => {
    const data = await create(dataUrl.url_origin)
    expect(data.code).to.equals(200)
    expect(data.data._id).to.not.equals('')
  })
})
