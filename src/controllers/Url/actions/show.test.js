'use stricts'
const { after, before, describe } = require('mocha')
const { expect } = require('chai')
const show = require('./show')
const { UrlModel } = require('../../../models/')

describe('controllers/UrlController/actions/show', () => {
  const dataUrl = {
    url_origin: 'http://localhost/slashslashlaurlmaslargadelmundo',
    url_shorter: 'http://shorter.cl/aabbcc'
  }

  before(async () => {
    await UrlModel.deleteMany()
    await new UrlModel(dataUrl).save()
  });

  after(async () => {
    await UrlModel.deleteMany()
  });


  it('expect an error', async () => {
    const data = await show('lala')
    expect(data.code).to.equals(404)
    expect(data.message).to.equals('Url no encontrada')
  })

  it('expect a success', async () => {
    const data = await show(dataUrl.url_shorter)
    expect(data.code).to.equals(200)
    expect(data.data.url_origin).to.equals(dataUrl.url_origin)
  })
})
