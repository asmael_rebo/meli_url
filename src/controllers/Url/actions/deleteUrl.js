const redis = require('../../../redis')
const {
  LogUrl,
  UrlModel
} = require('../../../models/')
const { findShortUrl } = require('../utils/')
const { responseAction } = require('../../../utils')
/**
 * @typedef {import('../../../utils/types').ResponseAction} ResponseAction
 */
/**
 * Genera una url corta en base a una url ingresada.
 * @example
 * const deleteUrl = deleteUrl(urlOrigen)
 * @returns {Promise<ResponseAction>} Respuesta borrado
 */
module.exports = async (shortUrl) => {
  try {
    const getUrl = await findShortUrl(shortUrl)
    if (getUrl) {
      UrlModel.deleteOne({ url_shorter: shortUrl }).exec()
      // TODO: limpiar key cache redis
      const message = 'Url eliminada con éxito'
      const data = {
        url_origin: getUrl.url_origin,
        url_shorter: getUrl.url_shorter,
        log_type: 'deleted'
      }
      // eliminar cache
      await redis.del(shortUrl)
      // Generar log
      const saveLog = new LogUrl(data).save()
      if (!saveLog) {
        message += '. No se guardo el log.'
      }
      return responseAction(200, message, { data: getUrl })
    } else {
      return responseAction(404, 'Url no encontrada')
    }
  } catch (error) {
    throw new Error('error al eliminar url')
  }
}
