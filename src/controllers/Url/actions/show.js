const redis = require('../../../redis')
const { findShortUrl } = require('../utils')
const { responseAction } = require('../../../utils')
/**
 * @typedef {import('../../../utils/types').ResponseAction} ResponseAction
 */
/**
 * Devuelve url corta en base a url larga.
 * @example
 * const goUrl = goUrl(urlCorta)
 * @returns {Promise<ResponseAction>} Url larga
 */
module.exports = async (shortUrl) => {
  try {
    const getUrl = await findShortUrl(shortUrl)
    if (!getUrl) {
      return responseAction(404, 'Url no encontrada')
    } else {
      return responseAction(200, 'Url obtenida con éxito', { data: getUrl })
    }
  } catch(error) {
    throw new Error('error al redireccionar a url')
  }
}
