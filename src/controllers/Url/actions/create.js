const redis = require('../../../redis')
const { UrlModel } = require('../../../models/')
const {
  findOriginUrl,
  generateShortUrl
} = require('../utils/')
const {
  responseAction,
  validateUrlFormat
} = require('../../../utils/')
/**
 * @typedef {import('../../../utils/types').ResponseAction} ResponseAction
 */
/**
 * Genera una url corta en base a una url ingresada.
 * @example
 * const newUrl = createUrl(urlOrigen)
 * @returns {Promise<ResponseAction>} Nueva url
 */
module.exports = async (urlOrigin) => {
  try {
    if (!validateUrlFormat(urlOrigin)) {
      throw new Error('Formato url incorrecto')
    }
    let code = 200
    let message = 'Url generada con éxito'
    let data
    // Se busca si url existe en bd
    const getUrl = await findOriginUrl(urlOrigin)
    if (!getUrl) {
      // Genera url nueva
      const shortUrl = await generateShortUrl()
      data = {
        url_origin: urlOrigin,
        url_shorter: shortUrl
      }

      // Guarda url en bd
      const newUrl = new UrlModel(data).save()
      if (!newUrl) {
        code = 404
        message = 'Unable to save url in database'
      }
      // Generar cache redis
      redis.set(shortUrl, urlOrigin)
    } else {
      // Responde url encontrada
      data = getUrl
    }
    // TODO: Generar cache redis
    return responseAction(code, message, { data })
  } catch(error) {
    throw new Error(error.message)
  }
}
