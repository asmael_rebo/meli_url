'use stricts'
const { after, before, describe } = require('mocha')
const { expect } = require('chai')
const redirect = require('./redirect')
const { UrlModel } = require('../../../models/')

describe('controllers/UrlController/actions/redirect', () => {
  const dataUrl = {
    url_origin: 'http://localhost/slashslashlaurlmaslargadelmundo',
    url_shorter: 'http://shorter.cl/aabbcc'
  }

  before(async () => {
    await UrlModel.deleteMany()
    await new UrlModel(dataUrl).save()
  });

  after(async () => {
    await UrlModel.deleteMany()
  });


  it('expect an error', async () => {
    const data = await redirect('lala')
    expect(data.code).to.equals(404)
    expect(data.message).to.equals('Url no encontrada')
  })

  it('expect a success', async () => {
    const data = await redirect(dataUrl.url_shorter)
    expect(data.code).to.equals(301)
    expect(data.data.url_origin).to.equals(dataUrl.url_origin)
  })
})
