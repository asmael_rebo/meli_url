const { describe } = require('mocha')
const { expect } = require('chai')
const action = require('./types')

describe('controllers/LogUrlController', () => {
  it('expect a success list', () => {
    const data = action.TypeLogs()
    expect(data.code).to.equals(200)
  })
})
