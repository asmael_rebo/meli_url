const {
  LogUrl
} = require('../../../models/')
const { logTypes } = require('../../../constants')
/**
 * @typedef {import('../../../utils/types').ResponseAction} ResponseAction
 */
/**
 * Retorna los tipos de logs disponibles
 * @example
 * const logs = GetLog()
 * @returns {Promise<ResponseAction>} Log con datos requeridos
 */
exports.GetLog = async function (log_type) {
  const types = logTypes.types
  if (!types.hasOwnProperty(log_type)) {
    return {
      code: 404,
      status: 'error',
      results: 0,
      message: 'Tipo de reporte no existe'
    }
  } else {
    let results = []
    if (log_type !== 'enabled') {
      results = await LogUrl.aggregate([
        {
          $match: {
            log_type,
          },
        },
        {
          $group: {
            _id: "$url_shorter",
            count: {
              $sum: 1,
            }
          }
        }
      ])
    } else {
      results = await LogUrl.aggregate([
        {
          $lookup: {
            from: "urls", // collection name in db
            localField: "url_shorter",
            foreignField: "url_shorter",
            as: "urls"
          }
        },
        {
          $match: {
            log_type: 'visit',
            urls: {
              $ne: []
            }
          },
        },
        {
          $group: {
            _id: "$url_shorter",
            count: {
              $sum: 1,
            }
          }
        }
      ])
    }
    return {
      code: 200,
      status: 'success',
      results: results.length,
      message: 'Cantidad total agrupada por url',
      data: {
        data: results
      }
    }
  }
}
