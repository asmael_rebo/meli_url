'use stricts'
const { describe } = require('mocha')
const { expect } = require('chai')
const { GetLog } = require('./get')

describe('controllers/LogUrlController/getLog', () => {
  it('expect an error', async () => {
    const data = await GetLog('lala')
    expect(data.code).to.equals(404)
    expect(data.message).to.equals('Tipo de reporte no existe')
  })
  it('expect a success', async () => {
    const data = await GetLog('enabled')
    expect(data.code).to.equals(200)
  })
  it('expect a success', async () => {
    const data = await GetLog('visit')
    expect(data.code).to.equals(200)
  })
})
