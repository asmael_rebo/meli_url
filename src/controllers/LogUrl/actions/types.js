const { logTypes } = require('../../../constants')
/**
 * @typedef {import('../../../utils/types').ResponseAction} ResponseAction
 */
/**
 * Retorna los tipos de logs disponibles
 * @example
 * const types = TypeLogs()
 * @returns {ResponseAction} Respuesta
 */
exports.TypeLogs = function () {
    try {
      const types = logTypes.types
      return {
        code: 200,
        status: 'success',
        results: types.length,
        message: 'Tipos de reportes disponibles',
        data: {
          data: types
        }
      }
    } catch(error) {
      return {
        code: 400,
        status: 'error',
        results: 0,
        message: 'Error',
        data: {}
      }
    }
};
