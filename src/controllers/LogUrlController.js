const { TypeLogs } = require('./LogUrl/actions/types')
const { GetLog } = require('./LogUrl/actions/get')
/**
 * @typedef {import('../models/types').ShorterUrlAttributes} ShorterUrlAttributes
 */
exports.types = function (req, res) {
    try {
      const types = TypeLogs()
      res.status(types.code).json(types)
    } catch(err) {
      res.status(404).json(
        {
          code: 404,
          error: err.message
        }
      )
    }
};

exports.getLog = async function(req, res) {
  try {
    const log_type = req.params.type
    const logs = await GetLog(log_type)
    res.status(logs.code).json(logs)
  } catch(err) {
    res.status(404).json(
      {
        code: 404,
        error: err.message
      }
    )
  }
}
