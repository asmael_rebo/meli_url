const {
  create,
  deleteUrl,
  redirect,
  show
} = require('./Url/actions/')
const { validateFields } = require('../utils/')
const { URL_SHORT_BASE } = process.env
/**
 * @typedef {import('../models/types').ShorterUrlAttributes} ShorterUrlAttributes
 */
exports.createUrl = async function (req, res) {
  try {
    validateFields(req.body, ['url'])
    const { body: { url: urlOrigin } } = req
    const data = await create(urlOrigin)
    res.status(data.code).json(data)
  } catch(err) {
    res.status(404).json(
      {
        code: 404,
        error: err.message
      }
    )
  }
}

exports.goUrl = async function(req, res) {
  try {
    validateFields(req.params, ['id'])
    const shortUrl = `${URL_SHORT_BASE}${req.params.id}`
    const data = await show(shortUrl)
    res.status(data.code).json(data)
  } catch(err) {
    res.status(404).json(
      {
        code: 404,
        error: err.message
      }
    )
  }
}

exports.redirectUrl = async function(req, res) {
  try {
    validateFields(req.params, ['id'])
    const shortUrl = `${URL_SHORT_BASE}${req.params.id}`
    const data = await redirect(shortUrl)
    // Si es 301 se agrega información de redireccionamiento
    if (data.code === 301) {
      res.redirect(data.code, data.data.url_origin)
    } else {
      res.status(data.code).json(data)
    }
  } catch(err) {
    res.status(404).json(
      {
        code: 404,
        error: err.message
      }
    )
  }
}

exports.deleteUrl = async function(req, res) {
  try {
    validateFields(req.params, ['id'])
    const shortUrl = `${URL_SHORT_BASE}${req.params.id}`
    const data = await deleteUrl(shortUrl)
    res.status(data.code).json(data)
  } catch(err) {
    res.status(404).json(
      {
        code: 404,
        error: err.message
      }
    )
  }
}
