const UrlModel = require('./UrlModel')
const LogUrl = require('./LogUrl')

module.exports = {
  UrlModel,
  LogUrl
}
