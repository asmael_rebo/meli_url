/** Tipo que representa los atributos del Modelo shorterUrl */
export interface ShorterUrlAttributes {
  /** URL Origen */
  url_origin: string
  /** URL Corta */
  url_shorter: string
}
