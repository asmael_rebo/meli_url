require('../db');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LogUrl = new Schema ({
    url_origin: { type: String, required: true },
    url_shorter: { type: String, required: true },
    log_type: {
      type: String,
      enum : ['visit','deleted'],
      default: 'visit'
    },
}, { timestamps: true });

module.exports = mongoose.model('log_urls', LogUrl)
