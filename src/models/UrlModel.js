require('../db');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * @class ShorterUrl
 */
const ShorterUrl = new Schema ({
    url_origin: { type: String, required: true },
    url_shorter: { type: String, required: true },
}, { timestamps: true });

module.exports = mongoose.model('urls', ShorterUrl)
