const { Router } = require('express')
const router = Router()
const {
  createUrl,
  deleteUrl,
  goUrl,
  redirectUrl
} = require('../controllers/UrlController')

router.route('/:id').get(redirectUrl)
router.route('/generate').post(createUrl)
router.route('/:id/data').get(goUrl)
router.route('/:id').delete(deleteUrl)

module.exports = router
