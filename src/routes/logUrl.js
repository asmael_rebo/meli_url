const express = require('express');
const router = express.Router();
const LogUrlController = require('../controllers/LogUrlController');

router.route('/').get(LogUrlController.types);

router.route('/:type').get(LogUrlController.getLog)

module.exports = router;
