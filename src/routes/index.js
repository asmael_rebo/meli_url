const shorterUrl = require('./shorterUrl')
const logUrl = require('./logUrl')

module.exports = app => {
  app.use('/logs', logUrl)
  app.use('/', shorterUrl)
}
