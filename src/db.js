const mongoose = require('mongoose');

const {
  MONGO_USERNAME,
  MONGO_PASSWORD,
  MONGO_HOSTNAME,
  MONGO_PORT,
  MONGO_DB,
  MONGO_TEST_USERNAME,
  MONGO_TEST_PASSWORD,
  MONGO_TEST_HOSTNAME,
  MONGO_TEST_PORT,
  MONGO_TEST_DB,
} = process.env;
const env = process.env.NODE_ENV || 'development';

const options = {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true,
  family: 4
};
let url = ''
if (env === 'development') {
  url = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`;
} else if(env === 'test') {
  url = `mongodb://${MONGO_TEST_USERNAME}:${MONGO_TEST_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_TEST_PORT}/${MONGO_TEST_DB}?authSource=admin`;
} else if(env === 'stage') {
  url = `mongodb+srv://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}/${MONGO_DB}?retryWrites=true&w=majority`;
}
console.log(`Environment: ${env}`)
if (url) {
  mongoose.connect(url, options).then( function() {
    console.log('MongoDB is connected');
  })
  .catch( function(err) {
    console.log(`Cant connect to: ${url}`)
    console.log(err);
  });
} else {
  throw new Error('No existe url de conexión a la base de datos')
}
