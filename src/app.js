'use strict'

const express = require('express')
const router = require('./routes')
// Constantes
const PORT = process.env.PORT || 3000
const HOST = '0.0.0.0'

const app = express()
app.use(require('morgan')('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
// Rutas
router(app)
app.listen(PORT, HOST)
console.log(`Running on http://${HOST}:${PORT}`)
