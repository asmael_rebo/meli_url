const { describe } = require('mocha')
const { expect } = require('chai')
const action = require('./validateFields')

describe('utils/validateFields', () => {
  it('expect a missing field error', () => {
    try {
      action({}, ['url'])
    } catch (err) {
      expect(err.message).to.equals('Missing fields on request')
    }
  })
  it('expect a missing field error', () => {
    try {
      action({lala: 'lolo'}, ['url'])
    } catch (err) {
      expect(err.message).to.equals('Missing fields on request')
    }
  })
  it('expect a success', async () => {
    const data = action({url: 'lala'}, ['url'])
    expect(data).to.be.true
  })
})
