const { default: ShortUniqueId } = require('short-unique-id');
const {
    URL_SHORT_BASE,
    URL_LENGTH
  } = process.env;

/**
 * Metodo para acortar una URL.
 *
 * @param {string} url - URL.
 * @returns {string} - ID de URL corta.
 * @example const id = await shorterUrl('https://lasuperurllarga.com/lalalal');
 */
module.exports = () => {
    const uid = new ShortUniqueId()
    const id = URL_SHORT_BASE + uid.randomUUID(URL_LENGTH)
    return id
}
