/**
 * Valida que un string tenga formato url(http y https)
 * @param {string} string Texto a validar
 * @example
 * validateUrlFormat('string')
 * @returns {boolean} True si es url, false sino
 */
module.exports = (string) => {
  let url;
  try {
    url = new URL(string);
  } catch (_) {
    return false;
  }

  return url.protocol === "http:" || url.protocol === "https:";
}
