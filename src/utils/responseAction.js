/**
 * @typedef {import('./types').ResponseAction} ResponseAction
 */
/**
 * Genera respuesta formateada
 * @param {number} code Codigo de respuesta
 * @param {string} message Mensaje de respuesta
 * @param {any} [extras] Data complementaria para la respuesta
 *
 * @example
 * const response = ResponseAction(200, 'holi', {data: {}})
 * @returns {ResponseAction} Respuesta formateada.
 */
module.exports = (code, message, extras = {}) => {
  return {
    code,
    success: (code >= 400) ? false : true,
    message,
    ...extras
  }
}
