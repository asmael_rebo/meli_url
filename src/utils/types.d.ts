/** Tipo de respuesta base de las acciones del controlador */
export interface ResponseAction {
  /** Codigo de respuesta */
  code: number
  /** Estado de la respuesta */
  success: boolean
  /** Cantidad de resultados */
  results?: number
  /** Mensaje asociado a la respuesta */
  message?: string
  /** Data asociada a la respuesta */
  data?: any
}
