module.exports = (body, fields = []) => {
  const iterateFields = Object.keys(body)
  let found = 0
  for (const field of iterateFields) {
    if (fields.includes(field)){
      found++
    }
  }
  if (found !== fields.length) {
    throw new Error('Missing fields on request')
  }
  return true
}
