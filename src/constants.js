module.exports = {
  logTypes: {
    types: {
      visit: 'Listado de todas las urls con su conteo de visitas',
      deleted: 'Listado de todas las urls eliminadas',
      enabled: 'Listado de todas las urls disponibles en este momento con su conteo de visitas',
    }
  }
}
